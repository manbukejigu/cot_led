/**
  **********************************************************************************************************************
  * @file    cot_led.h
  * @brief   该文件提供LED灯控制功能函数原型
  * @author  const_zpc    any question please send mail to const_zpc@163.com
  * @version V1.0.0
  * @date    2023-11-05
  **********************************************************************************************************************
  *
  **********************************************************************************************************************
  */

/* Define to prevent recursive inclusion -----------------------------------------------------------------------------*/
#ifndef _COT_LED_H_
#define _COT_LED_H_

/* Includes ----------------------------------------------------------------------------------------------------------*/
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

/* Exported types ----------------------------------------------------------------------------------------------------*/

// 状态bit位所使用的byte数，bit之间的控制时间为时间颗粒度
#define LED_STATE_BYTE_NUM       4

/**
  * @brief  LED亮灭枚举定义
  */
typedef enum{
    COT_LED_OFF = 0,                         	/*!< (0)灯灭 */
    COT_LED_ON = !COT_LED_OFF                   /*!< (1)灯亮 */
} cotLedState_e;

typedef void (*cotLedCtrl_f)(cotLedState_e state);
typedef uint16_t led_t;

typedef struct
{
    uint8_t validBits;                          /*!< 状态有效bit位 */
    uint8_t offset;                             /*!< 状态bit位偏移 */
    uint8_t count;                              /*!< 控制次数 */
    uint8_t isSetCount : 1;                     /*!< 是否设置了控制次数 */
    uint8_t defState : 1;                       /*!< 默认状态 */
    uint8_t curState : 1;                       /*!< 当前状态 */
    uint8_t pwmDir : 1;                         /*!< PWM增减方向 */
    uint8_t isPwm : 1;                          /*!< PWM模式 */
    uint16_t tic;                               /*!< 时间计数器 */
    uint16_t interval;                          /*!< 每次控制的时间颗粒度，单位为1ms */
    union
    {
        struct
        {
            uint16_t onTime;                    /*!< 亮的时长 */
            uint16_t tic;                       /*!< PWM计时 */
        } pwm;
        uint8_t state[LED_STATE_BYTE_NUM];      /*!< 状态bit位 */
    } data;
} cotLedProc_t;

typedef struct
{
    cotLedProc_t proc;
    cotLedCtrl_f pfnLedCtrl;
} cotLedCfg_t;

/* Exported constants ------------------------------------------------------------------------------------------------*/
/* Exported macro ----------------------------------------------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------------------------------------------------*/

extern int cotLed_Init(cotLedCfg_t pCfgTable[], size_t num);

extern int cotLed_SetState(led_t led, cotLedState_e state);
extern int cotLed_SetStateWithTime(led_t led, cotLedState_e state, uint16_t time);

extern int cotLed_ON(led_t led);
extern int cotLed_OFF(led_t led);
extern int cotLed_Toggle(led_t led);

extern int cotLed_Twinkle(led_t led, uint16_t time);
extern int cotLed_TwinkleWithCount(led_t led, uint16_t time, uint8_t count, cotLedState_e defState);

extern int cotLed_Breathe(led_t led, uint16_t period);
extern int cotLed_BreatheWithCount(led_t led, uint16_t period, uint8_t count, cotLedState_e defState);

extern int cotLed_Custom(led_t led, ...);
extern int cotLed_CustomWithCount(led_t led, uint8_t count, cotLedState_e defState, ...);

extern int cotLed_Marquee(led_t led[], uint8_t ledNum, int32_t time);
extern int cotLed_MarqueeWithCount(led_t led[], uint8_t ledNum, int32_t time, uint8_t count, cotLedState_e defState);
extern int cotLed_Waterfall(led_t led[], uint8_t ledNum, int32_t time);
extern int cotLed_WaterfallWithCount(led_t led[], uint8_t ledNum, int32_t time, uint8_t count, cotLedState_e defState);

extern int cotLed_Ctrl(uint32_t sysTime);


#endif
