#include "timer.h"
#include "cot_led.h"

typedef enum
{
    LED_0 = 0,
    LED_1,
    LED_2,
  
    /* 勿删除,用来统计LED的数目 */
    LED_MAX_NUM
} LedType_e;

static void InitLedIo(void)
{ 
    GPIO_InitTypeDef  GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    GPIO_SetBits(GPIOB, GPIO_Pin_5);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOE, &GPIO_InitStructure);
    GPIO_SetBits(GPIOE, GPIO_Pin_5);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    GPIO_SetBits(GPIOD, GPIO_Pin_5);
}

void CtrlLed1(cotLedState_e state)
{
    state == COT_LED_ON ? GPIO_ResetBits(GPIOB, GPIO_Pin_5) :  GPIO_SetBits(GPIOB, GPIO_Pin_5);
}

void CtrlLed2(cotLedState_e state)
{
    state == COT_LED_ON ? GPIO_ResetBits(GPIOE, GPIO_Pin_5) :  GPIO_SetBits(GPIOE, GPIO_Pin_5);
}

void CtrlLed3(cotLedState_e state)
{
    state == COT_LED_ON ? GPIO_SetBits(GPIOD, GPIO_Pin_5) :  GPIO_ResetBits(GPIOD, GPIO_Pin_5);
}

void FML_LED_Init(void)
{
    static cotLedCfg_t s_ledTable[LED_MAX_NUM] = {
        {.pfnLedCtrl = CtrlLed1},
        {.pfnLedCtrl = CtrlLed2},
        {.pfnLedCtrl = CtrlLed3},
    };

    InitLedIo();
    cotLed_Init(s_ledTable, LED_MAX_NUM);
}

uint32_t GetTimerMs(void)
{
    timeval_t ts;
    ReadSystemTime(&ts);
    return (ts.tv_msec);
}

int main(void)
{
    FML_LED_Init();

    // 2 秒内 3 次快闪，总共5次，次数完成后灯灭
    cotLed_CustomWithCount(LED_0, 5, COT_LED_OFF, 100, -100, 100, -100, 100, -100, -1400, 0);

    IoLedType_e led[] = {LED_1, LED_2};

    cotLed_Waterfall(led, 2, 300);  // 流水灯，不限次数

    while (1)
    {
        if (g_20ms)
        {
            g_20ms = 0;
            cotLed_Ctrl(GetTimerMs());
        }
    }
}
