# 轻量级LED控制模块

轻量级的LED控制软件框架，可以十分方便地控制LED的各种状态，甚至多个LED组合控制，如跑马灯等。

#### 特点

轻量级的LED控制模块代码：

* 移植方便，无需修改，只需要初始化提供读取相关IO 状态写入函数即可
* 丰富的接口实现，
  * 可以实现单个LED亮灭、翻转、闪烁、呼吸灯、自定义（如多少秒快闪几次等）等多种要求的功能
  * 可以实现多个LED组合跑马灯、流水灯等功能
  * 同时支持上述模式的次数设置等
* 代码量少，且代码注释丰富，风格统一，便于阅读和使用
* 如果设置某个LED为呼吸灯模式，则需要保证 `cotLed_Ctrl`调用周期为1毫秒（优先级需要最高，或者定时器调度效果最好）
* 非阻塞任务，因此裸机和操作系统都适用
  * 操作系统下非线程安全，最好可以使用读写锁，如果没有读写锁则至少使用互斥锁

#### 软件架构

##### 文件介绍

* cot_led.c 和 cot_led.h

  > 实现具体功能，对外提供的 API 接口函数
  >

#### 使用说明

1. 使用前初始化函数 `cotLed_Init`，添加所有指示灯
   > 需要实现写入IO状态的函数，然后初始化结构体的 `pfnLedCtrl`即可。
   >
2. 周期调用函数 `cotLed_Ctrl`, 入参为毫秒级的系统时长，用来具体控制LED状态
3. 调用相关函数设置灯光效果
   > 不支持重复调用，重复调用设置函数相关信息会复位
   >

##### 初始化

```c
typedef enum
{
    LED_0 = 0,
    LED_1,
    LED_2,
  
    /* 勿删除,用来统计LED的数目 */
    LED_MAX_NUM
} LedType_e;

static void InitLedIo(void)
{ 
    GPIO_InitTypeDef  GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    GPIO_SetBits(GPIOB, GPIO_Pin_5);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOE, &GPIO_InitStructure);
    GPIO_SetBits(GPIOE, GPIO_Pin_5);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    GPIO_SetBits(GPIOD, GPIO_Pin_5);
}

void CtrlLed1(cotLedState_e state)
{
    state == COT_LED_ON ? GPIO_ResetBits(GPIOB, GPIO_Pin_5) :  GPIO_SetBits(GPIOB, GPIO_Pin_5);
}

void CtrlLed2(cotLedState_e state)
{
    state == COT_LED_ON ? GPIO_ResetBits(GPIOE, GPIO_Pin_5) :  GPIO_SetBits(GPIOE, GPIO_Pin_5);
}

void CtrlLed3(cotLedState_e state)
{
    state == COT_LED_ON ? GPIO_SetBits(GPIOD, GPIO_Pin_5) :  GPIO_ResetBits(GPIOD, GPIO_Pin_5);
}

void FML_LED_Init(void)
{
    static cotLedCfg_t s_ledTable[LED_MAX_NUM] = {
        {.pfnLedCtrl = CtrlLed1},
        {.pfnLedCtrl = CtrlLed2},
        {.pfnLedCtrl = CtrlLed3},
    };

    InitLedIo();
    cotLed_Init(s_ledTable, LED_MAX_NUM);
}

```

##### 任务执行

周期调用函数 `cotLed_Ctrl` ，入参为毫秒累计计时。

```c
void LedTask(void)
{
    uint32_t time;
  
    while (1)
    {
        cotLed_Ctrl(GetTimerMs());
        cotOs_WaitFor(1);
    }
}

// 或者
int main(void)
{
    int time1ms_cnt = 0;

    while(1)
    {
        if (flag_1ms)
        {
            flag_1ms = 0;

            cotLed_Ctrl(time1ms_cnt);
            time1ms_cnt++;
        }
    }
}
```

##### 灯光设置

```c
/************ 普通模式  ***********/
cotLed_SetState(LED_0, COT_LED_OFF); // 设置灯灭
cotLed_SetState(LED_0, COT_LED_ON); // 设置灯亮

cotLed_SetStateWithTime(LED_0, COT_LED_ON, 1000); // 设置灯亮持续一秒

/************ 闪烁模式  ***********/
cotLed_Twinkle(LED_0, 500);  // 500毫秒进行闪烁（亮灭间隔时间）
cotLed_TwinkleWithCount(LED_0, 500, 3, COT_LED_OFF);  // 500毫秒进行闪烁3次（一亮一灭为一次），次数完成后灯灭

/************ 呼吸灯模式（软件模拟PWM）  ***********/
cotLed_Breathe(LED_0, 2000);  // 2秒完成一次呼吸亮灭操作
cotLed_BreatheWithCount(LED_0, 2000, 3, COT_LED_OFF);  // 2秒完成一次呼吸亮灭操作，总共3次，次数完成后灯灭

/************ 自定义模式  ***********/
// 2 秒内 3 次快闪，总共5次，次数完成后灯灭
cotLed_CustomWithCount(LED_0, 5, COT_LED_OFF, 100, -100, 100, -100, 100, -100, -1400, 0);

// 2 秒内 1 次快闪，无限制次数
cotLed_Custom(LED_0, 100, -1900, 0);

/************ 多个LED控制模式  ***********/
IoLedType_e led[] = {LED_0, LED_1, LED_2};

cotLed_Waterfall(led, 3, 300);  // 流水灯，不限次数
cotLed_MarqueeWithCount(led, 3, 500, 3, COT_LED_OFF); // 跑马灯执行3次

```

#### 示例工程代码

代码链接：[stm32 工程](https://gitee.com/cot_package/demo_stm32)

#### 关于作者

1. CSDN 博客 [大橙子疯](https://blog.csdn.net/qq_24130227?spm=1010.2135.3001.5343)
2. 联系邮箱 `const_zpc@163.com`
3. 了解更多可关注微信公众号

![大橙子疯嵌入式](微信公众号.jpg)
